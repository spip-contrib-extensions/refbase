<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'label_css' => 'CSS to add (css). Example: <i>font-size:1.25em;</i>.',
'label_doublons' => 'Show duplicates (showDups):',
'label_liens' => 'Show extern links (showLinks):',
'label_liens_exports' => 'Shows abstract and export options (showAbstract):',
'label_max' => 'Number of shown results (showRows):',
'label_style' => 'Citation style (citeStyle):',
'label_tri' => 'Citations order (citeOrder):',
'label_tri_annee' => 'Year (year)',
'label_tri_auteur' => 'Author (author)',
'label_tri_type' => 'Type (type)',
'label_tri_typeannee' => 'By type then by year (type-year)',
'label_tri_datecreation' => 'Creation date (creation-date)',
'label_url_refbase' => 'refbase URL with a final / (url_refbase):',
'label_vue' => 'View (submit):',
'label_vue_citations' => 'Citations (Cite)',
'label_vue_liste' => 'List view (List)',
'label_vue_details' => 'Details (Display)',
'comportement_citations' => 'Default parameters (citations view only)',
'comportement_defaut' => 'Default parameters',
'config_refbase' => 'refbase',
'non' => 'no',
'option_style' => 'Advanced style option (facultative)',
'oui' => 'yes'

);
?>
